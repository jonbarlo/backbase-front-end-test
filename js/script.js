	// create the module and name it scotchApp
	var scotchApp =
				angular.module('myBackbaseTestApp', ['ngRoute'])
				//.constant('API_BASE_URL', 'http://api.openweathermap.org/data/2.5/forecast?id=')
				.constant('API_BASE_URL', 'http://api.openweathermap.org/data/2.5/forecast?q=')
				.constant('API_TOKEN_ID', '&appid=67c890844b60522c4737dfecd1ed1459')
				.constant('EU_SYMBOL', 'EU')
				.constant('EU_CITIES', 'Amsterdam,Stockholm,Reykjavik,Helsinki,Oslo')
				//.constant('EU_CODES', '2759794,2673730,3413829,658225,6453366')
	;

	// configure our routes
	scotchApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'views/pages/home.html',
				controller  : 'mainController'
			})

			// route for the about page
			.when('/about', {
				templateUrl : 'views/pages/about.html',
				controller  : 'aboutController'
			})

			// route for the contact page
			.when('/contact', {
				templateUrl : 'views/pages/contact.html',
				controller  : 'contactController'
			});
	});

	scotchApp.directive('selectedForecastSet', function () {
		return {
			restrict: 'EA', //E = element, A = attribute, C = class, M = comment         
			scope: {
				//@ reads the attribute value, = provides two-way binding, & works with functions
				title: '@'         },
				templateUrl: 'views/directives/myDirective.html',
				controller  : 'mainController',
				scope: {
					mySelectedWeatherData: '=info'
				},
				//scope: { /* Isolate scope, no $rootScope access anymore */ },
				//controller: controllerFunction, //Embed a custom controller in the directive
				//link: function ($scope, element, attrs) { } //DOM manipulation
			}
	});

	scotchApp.service('WeatherService', function ($q, $http, $rootScope, API_BASE_URL, API_TOKEN_ID, EU_SYMBOL) {
		//
		this.getForecast = function(city)
		{
			//console.log("Trying to call API");console.log(API_BASE_URL+city+','+EU_SYMBOL+API_TOKEN_ID);
			//promise
			var deferred = $q.defer();
			$http({
				method: 'GET',
				dataType: "json",
				url: API_BASE_URL+city+','+EU_SYMBOL+API_TOKEN_ID
			})
			.then(function onSuccess(response) {
				// Handle success
				var data = response.data;
				var status = response.status;
				var statusText = response.statusText;
				var headers = response.headers;
				var config = response.config;
				//deferred.resolve(true);
				$rootScope.globalWeatherData = data;
				console.log(data);
				return data;
				//
			}).catch(function onError(response) {
				// Handle error
				var data = response.data;
				var status = response.status;
				var statusText = response.statusText;
				var headers = response.headers;
				var config = response.config;
				deferred.reject();
			});
			return deferred.promise;
		};
		//
	});

	// create the controller and inject Angular's $scope
	scotchApp.controller('mainController', function($scope, $rootScope, EU_CITIES, WeatherService) {
		$scope.selectedTimeRangeIndex = 0;
		$scope.cities = EU_CITIES.split(",");
		console.log($scope.selectedTimeRangeIndex);
		//
		$scope.data = {
			euCities: EU_CITIES, 
			selectedCity: null
		};

		//
		$scope.getWeatherForecast = function() {
			console.log("getWeatherForecast");
			console.log($scope.data.selectedCity);
			$scope.showDetails = true;
            WeatherService.getForecast($scope.data.selectedCity).then(
              function(data) {
				  console.log("weatherData");
                  console.log(data);
				  $rootScope.globalWeatherData = data;
					console.log("$rootScope.globalWeatherData---->");
					console.log($rootScope.globalWeatherData);
				  $scope.changeForecastSet($scope.selectedTimeRangeIndex,$rootScope.globalWeatherData.list.length)
              });
		};

		$scope.changeForecastSet = function(index, size) {
			console.log("$changeForecastSet.---->");
			console.log(index);
			console.log(size);

			$scope.selectedTimeRangeIndex = index;
			if($scope.selectedTimeRangeIndex >= size)
				$scope.selectedTimeRangeIndex = size-1;

			if($scope.selectedTimeRangeIndex < 0)
				$scope.selectedTimeRangeIndex = 0;
				

		};

	});